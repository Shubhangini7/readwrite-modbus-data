/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javafxapplication10;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.ModbusSlaveException;
import net.wimpi.modbus.io.ModbusTCPTransaction;
import net.wimpi.modbus.msg.ReadMultipleRegistersRequest;
import net.wimpi.modbus.msg.ReadMultipleRegistersResponse;
import net.wimpi.modbus.msg.WriteMultipleRegistersRequest;
import net.wimpi.modbus.msg.WriteMultipleRegistersResponse;
import net.wimpi.modbus.net.TCPMasterConnection;
import net.wimpi.modbus.procimg.Register;
import net.wimpi.modbus.procimg.SimpleRegister;

/**
 *
 * @author shubh
 */
public class JavaFXApplication10 extends Application {
    
    HashMap<Integer, Integer> v = new HashMap<>();
    Integer v1,v2,v3,v4;
TCPMasterConnection con = null; //the connection
            ModbusTCPTransaction trans = null; //the transaction
            ReadMultipleRegistersRequest req = null; //the request
            ReadMultipleRegistersResponse res = null; //the response
            WriteMultipleRegistersRequest reqW = null;
            WriteMultipleRegistersResponse resW = null;
            
    @Override
    public void start(Stage primaryStage) {
        TextField t1 = new TextField();
        TextField t2 = new TextField();
        TextField t3 = new TextField();
        TextField t4 = new TextField();
        TextField u1 = new TextField();
        TextField u2 = new TextField();
        TextField u3 = new TextField();
        TextField u4 = new TextField();
        
        Button btn = new Button();
        try{
                       /* Variables for storing the parameters */
            InetAddress addr = InetAddress.getByName("192.168.43.217"); //the slave's address
            int port = 5555;
            int ref = 0; //the reference; offset where to start reading from
            int count = 4; //the number of DI's to read
            int repeat = 0; //a loop for repeating the transaction
            
/*1. Setup the parameters
            if (args.length < 3) {
                System.exit(1);
            } else {
                try {
                    String astr = args[0];
                    //String astr = System.getProperty("args[0]");
                    //String astr = getParameters().getNamed().get("args[0]");
                    int idx = astr.indexOf(':');
                    if (idx > 0) {
                        port = Integer.parseInt(astr.substring(idx + 1));
                        astr = astr.substring(0, idx);
                    }
                    addr = InetAddress.getByName(astr);
                    ref = Integer.decode(args[1]).intValue();
                    count = Integer.decode(args[2]).intValue();
                    if (args.length == 4) {
                        repeat = Integer.parseInt(args[3]);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    System.exit(1);
                }
            }*/
//2. Open the connection
            con = new TCPMasterConnection(addr);
            con.setPort(port);
            con.connect();

//3. Prepare the request
            req = new ReadMultipleRegistersRequest(ref, count);
            
//4. Prepare the transaction
            trans = new ModbusTCPTransaction(con);
            trans.setRequest(req);
            

//5. Execute the transaction repeat times
         
    
            int k = 0;
            do {
                trans.execute();
                res = (ReadMultipleRegistersResponse) trans.getResponse();
                while(ref != count)
                {   
                    v.put(ref, res.getRegisterValue(ref));
                    //System.out.println("Digital Inputs =" + res.getRegisterValue(ref));
                    ref++;
                }
                k++;
            } while (k < repeat);

            
            int i = 0;
            TextField[] path = new TextField[] {t1,t2,t3,t4};
            Set keys = v.keySet();
            Iterator itr = keys.iterator();
 
            Integer key;
            Integer value;
            while(itr.hasNext())
            {
                key = (Integer)itr.next();
                value = (Integer)v.get(key);
                //v1.setText(value.toString());
                path[i].setText(value.toString());
                i++;
                System.out.println(key + " - "+ value);
            }

            v1 = Integer.valueOf(t1.getText());
            //v3 = Integer.valueOf(t3.getText());
            
            //write perform
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                v1 = Integer.valueOf(u1.getText());
                v2 = Integer.valueOf(u2.getText());
                v3 = Integer.valueOf(u3.getText());
                v4 = Integer.valueOf(u4.getText());
                
                t1.setText(v1.toString());
                t2.setText(v2.toString());
                t3.setText(v3.toString());
                t4.setText(v4.toString());
                
            Register[] regs = new SimpleRegister[4];
            regs[0] = new SimpleRegister(v1);
            regs[1] = new SimpleRegister(v2);
            regs[2] = new SimpleRegister(v3);
            regs[3] = new SimpleRegister(v4);
            reqW = new WriteMultipleRegistersRequest(0, regs);
            
            trans.setRequest(reqW);
                try {
                    trans.execute();
                } catch (ModbusSlaveException ex) {
                    Logger.getLogger(JavaFXApplication10.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ModbusException ex) {
                    Logger.getLogger(JavaFXApplication10.class.getName()).log(Level.SEVERE, null, ex);
                }
            resW = (WriteMultipleRegistersResponse) trans.getResponse();

            }
        });
            
           
            
            //6. Close the connection
            con.close();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        
        VBox vbox = new VBox(t1,t2,t3,t4);
        vbox.setMaxHeight(20);
        vbox.setMaxWidth(60);
        
        VBox vb = new VBox(u1,u2,u3,u4);
        vb.setMaxHeight(20);
        vb.setMaxWidth(60);
        
        btn.setText("update value");

        //ArrayList a = new ArrayList();
        //a.add(v1);
        
        //StackPane root = new StackPane();
        GridPane gridPane = new GridPane();
        //root.getChildren().add(vbox);
        //root.getChildren().add(vb);
        gridPane.add(vbox, 0,0,1,1);
        gridPane.add(vb, 1,0,1,1);
        gridPane.add(btn, 0,1,1,1);
        
        Scene scene = new Scene(gridPane, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
